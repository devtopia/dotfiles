export RBENV_ROOT=/usr/local/var/rbenv

# Tell ls to be colourful
export CLICOLOR=1
# export LSCOLORS=Exfxcxdxbxegedabagacad
# if background color is black
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx

# Tell grep to highlight matches
export GREP_OPTIONS='--color=auto'

export EDITOR=vim
export LANG=ja_JP.UTF-8
export LC_ALL=ja_JP.UTF-8

alias ll='ls -alhF'
alias vi='vim'
alias sudo='sudo -E '
alias remove_gems='gem uninstall -axI $(gem list --no-versions | egrep -v "bigdecimal|io-console|json|psych|rake|rdoc")'
