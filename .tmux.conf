# Tmux Plugin Manager
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-open'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-battery'
set -g @plugin 'tmux-plugins/tmux-online-status'
set -g @plugin 'tmux-plugins/tmux-cpu'
set -g @plugin 'tmux-plugins/tmux-net-speed'
set -g @plugin 'nhdaly/tmux-better-mouse-mode'
set -g @plugin 'samoshkin/tmux-plugin-sysstat'

# General settings
set -g default-terminal 'screen-256color'
set -g terminal-overrides 'xterm:colors=256'
set -gq utf8 on
bind r source-file ~/.tmux.conf\; display "Reloaded!"

# Set key bind of C-b to C-a
set -g prefix C-a
unbind C-b
bind C-a send-prefix

# Mouse settings
set -g mouse on

# Set refresh interval for status bar
set -g status-interval 5

if-shell 'test "$(uname)" = Darwin' "source-file ~/.tmux/osx.conf" "source-file ~/.tmux/linux.conf"

bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -r C-h select-window -t :-
bind -r C-l select-window -t :+

# Send a command to all panes
bind e setw synchronize-panes on
bind E setw synchronize-panes off

# auto start terminal
set -g @continuum-boot on
set -g @continuum-boot-options 'iterm'

# auto restore tmux
set -g @continuum-restore on

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
