# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = '2'

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.ssh.insert_key = false

  config.vm.define :dev do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'dev'
    config.vm.network :private_network, ip: '192.168.33.11'
    config.vm.network :forwarded_port, guest: 22, host: 2211, id: 'ssh'
    config.vm.network :forwarded_port, guest: 80, host: 8888
    config.vm.network :forwarded_port, guest: 443, host: 1443
    config.vm.network :forwarded_port, guest: 8090, host: 8090
    config.vm.provider :vmware_desktop do |v|
      v.vmx['memsize'] = '4096'
      v.vmx['numvcpus'] = '2'
    end
  end

  config.vm.define :staging do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'staging'
    config.vm.network :private_network, ip: '192.168.33.12'
    config.vm.network :forwarded_port, guest: 22, host: 2212, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :live do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'live'
    config.vm.network :private_network, ip: '192.168.33.13'
    config.vm.network :forwarded_port, guest: 22, host: 2213, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :gembox do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'gembox'
    config.vm.network :private_network, ip: '192.168.33.21'
    config.vm.network :forwarded_port, guest: 22, host: 2224, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :proxy do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'proxy'
    config.vm.network :private_network, ip: '192.168.33.31'
    config.vm.network :forwarded_port, guest: 22, host: 2231, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :db1 do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'db1'
    config.vm.network :private_network, ip: '192.168.33.32'
    config.vm.network :forwarded_port, guest: 22, host: 2232, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :db2 do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'db2'
    config.vm.network :private_network, ip: '192.168.33.33'
    config.vm.network :forwarded_port, guest: 22, host: 2233, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :db3 do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'db3'
    config.vm.network :private_network, ip: '192.168.33.34'
    config.vm.network :forwarded_port, guest: 22, host: 2234, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  config.vm.define :redmine do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'redmine'
    config.vm.network :private_network, ip: '192.168.33.41'
    config.vm.network :forwarded_port, guest: 22, host: 2241, id: 'ssh'
    config.vm.provider :vmware_desktop do |v|
      v.vmx['memsize'] = '2048'
      v.vmx['numvcpus'] = '2'
    end
  end

  config.vm.define :gitlab do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'gitlab'
    config.vm.network :private_network, ip: '192.168.33.51'
    config.vm.network :forwarded_port, guest: 22, host: 2251, id: 'ssh'
    config.vm.provider :vmware_desktop do |v|
      v.vmx['memsize'] = '2048'
      v.vmx['numvcpus'] = '2'
    end
  end

  config.vm.define :runner1 do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'runner1'
    config.vm.network :private_network, ip: '192.168.33.52'
    config.vm.network :forwarded_port, guest: 22, host: 2252, id: 'ssh'
    # config.vm.provision "shell", inline: <<-SHELL
    #   route del default
    #   route add default gw 192.168.0.1 eth1
    # SHELL
    # config.vm.provider :vmware_desktop
  end

  config.vm.define :runner2 do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'runner2'
    config.vm.network :private_network, ip: '192.168.33.53'
    config.vm.network :forwarded_port, guest: 22, host: 2253, id: 'ssh'
    config.vm.provider :vmware_desktop
  end

  # config.vm.define :docker do |config|
  #   config.vm.box = 'bento/ubuntu-18.04'
  #   config.vm.hostname = 'docker'
  #   config.vm.network :private_network, ip: '192.168.33.99'
  #   config.vm.network :forwarded_port, guest: 22, host: 2299, id: 'ssh'
  #   config.vm.provider :vmware_desktop do |v|
  #     v.vmx['memsize'] = '4096'
  #     v.vmx['numvcpus'] = '4'
  #   end
  # end

  config.vm.define :docker do |config|
    config.vm.box = 'bento/centos-7.6'
    config.vm.hostname = 'docker'
    config.vm.network :private_network, ip: '192.168.33.99'
    config.vm.network :forwarded_port, guest: 22, host: 2299, id: 'ssh'
    config.vm.provider :vmware_desktop do |v|
      v.vmx['memsize'] = '4096'
      v.vmx['numvcpus'] = '2'
    end
  end
end
