# -------------------------------------
# コマンド履歴を保存する。
# -------------------------------------

HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

# -------------------------------------
# zshのオプション
# -------------------------------------

# 補完機能の強化
autoload -Uz compinit; compinit

# 入力しているコマンド名が間違っている場合にもしかして：を出す。
setopt correct

# ビープを鳴らさない。
setopt no_beep

# 色を使う。
setopt prompt_subst

# ^Dでログアウトしない。
setopt ignore_eof

# ^Q/^Sのフローコントロールを無効にする。
setopt no_flow_control

# バックグラウンドジョブが終了したらすぐに知らせる。
setopt no_tify

# 直前と同じコマンドをヒストリに追加しない。
setopt hist_ignore_dups

# 補完
# タブによるファイルの順番切り替えをする。
setopt auto_menu

# cd -[tab]で過去のディレクトリにひとっ飛びできるようにする。
setopt auto_pushd

# ディレクトリ名を入力するだけでcdできるようにする。
setopt auto_cd

setopt localoptions shwordsplit

# -------------------------------------
# パス
# -------------------------------------

# 重複する要素を自動的に削除
typeset -U path cdpath fpath manpath

path=(
    $HOME/bin(N-/)
    /usr/local/bin(N-/)
    /usr/local/sbin(N-/)
    $path
)

# -------------------------------------
# プラグイン
# -------------------------------------

autoload -Uz history-search-end
autoload -Uz add-zsh-hook
autoload -Uz chpwd_recent_dirs cdr
autoload -Uz zmv
autoload -Uz promptinit; promptinit
autoload -Uz colors; colors
autoload -Uz vcs_info
autoload -Uz is-at-least

# メニュー選択モード
zstyle ":completion:*:default" menu select=2

# 大文字と小文字を区別しない
zstyle ":completion:*" matcher-list "m:{a-z}={A-Z}"

# cdr
add-zsh-hook chpwd chpwd_recent_dirs
zstyle ':chpwd:*' recent-dirs-max 200
zstyle ':chpwd:*' recent-dirs-default true

# # begin VCS
# zstyle ":vcs_info:*" enable git svn hg bzr
# zstyle ":vcs_info:*" formats "%F{green}(%s)-[%b]%f"
# zstyle ":vcs_info:*" actionformats "%F{red}(%s)-[%b|%a]%f"
# zstyle ":vcs_info:(svn|bzr):*" branchformat "%b:r%r"
# zstyle ":vcs_info:bzr:*" use-simple true
# zstyle ":vcs_info:*" max-exports 6
#
# if is-at-least 4.3.10; then
#   zstyle ":vcs_info:git:*" check-for-changes true # commitしていないのをチェック
#   zstyle ":vcs_info:git:*" stagedstr "<S>"
#   zstyle ":vcs_info:git:*" unstagedstr "<U>"
#   zstyle ":vcs_info:git:*" formats "%F{green}(%s)-[%b] %c%u%f"
#   zstyle ":vcs_info:git:*" actionformats "%F{red}(%s)-[%b|%a] %c%u%f"
# fi
#
# function _update_vcs_info_msg() {
#   LANG=en_US.UTF-8 vcs_info
#   RPROMPT="${vcs_info_msg_0_}"
# }
# add-zsh-hook precmd _update_vcs_info_msg
# # end VCS

# -------------------------------------
# プロンプト
# -------------------------------------

preexec() {
  echo -ne "\ek$1\e\\"
}
precmd() {
  echo -ne "\ek$(basename $SHELL)\e\\"
}
PROMPT="%(?.%{${fg_bold[green]}%}.%{${fg_bold[red]}%})[%n@%m:%~]%{${reset_color}%} %# "

# -------------------------------------
# エイリアス
# -------------------------------------

# -n 行数表示, -I バイナリファイル無視, svn関係のファイルを無視
alias grep="grep --color -n -I --exclude='*.svn-*' --exclude='entries' --exclude='*/cache/*'"

# ls
alias ls="ls -G" # color for darwin
alias l="ls -la"
alias la="ls -la"
alias l1="ls -1"

# tree
alias tree="tree -NC" # N: 文字化け対策, C:色をつける

# zmv
alias zmv="noglob zmv -W"

# tmux
alias tmux='tmux -2'

# -------------------------------------
# キーバインド
# -------------------------------------

bindkey -v

function cdup() {
  echo
  cd ..
  zle reset-prompt
}
zle -N cdup
bindkey '^k' cdup

# コマンド履歴をインクリメンタル検索する。
bindkey "^r" history-incremental-pattern-search-backward
bindkey "^s" history-incremental-pattern-search-forward

# コマンド履歴を検索する。
zle -N history-beginning-search-backward-end history-search-end
bindkey "^o" history-beginning-search-backward-end

# コマンド履歴から実行する。
bindkey '^x^r' anyframe-widget-execute-history

# 最近移動したディレクトリに移動する。
bindkey '^xb' anyframe-widget-cdr

# -------------------------------------
# その他
# -------------------------------------

# cdしたあとで、自動的に ls する
function chpwd() { ll }

# iTerm2のタブ名を変更する
function title {
  echo -ne "\033]0;"$*"\007"
}

# rbenv
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

# -------------------------------------
# zplug
# -------------------------------------
if [ -e /usr/local/opt/zplug/init.zsh ]; then
  export ZPLUG_HOME=/usr/local/opt/zplug
  source $ZPLUG_HOME/init.zsh
else
  export ZPLUG_HOME=/opt/homebrew/opt/zplug
  source $ZPLUG_HOME/init.zsh
fi
export ZSH_THEME='dracula'

# ダブルクォーテーションで囲うと良い
zplug "zsh-users/zsh-history-substring-search"

# コマンドも管理する
# グロブを受け付ける（ブレースやワイルドカードなど）
zplug "Jxck/dotfiles", as:command, use:"bin/{histuniq,color}"

# # こんな使い方もある（他人の zshrc）
# zplug "tcnksm/docker-alias", use:zshrc

# frozen タグが設定されているとアップデートされない
zplug "k4rthik/git-cal", as:command, frozen:1

# # GitHub Releases からインストールする
# # また、コマンドは rename-to でリネームできる
# zplug "junegunn/fzf-bin", \
#     from:gh-r, \
#     as:command, \
#     rename-to:fzf, \
#     use:"*darwin*amd64*"

# oh-my-zsh をサービスと見なして、
# そこからインストールする
zplug "plugins/git",   from:oh-my-zsh

# if タグが true のときのみインストールされる
zplug "lib/clipboard", from:oh-my-zsh, if:"[[ $OSTYPE == *darwin* ]]"

# # prezto のプラグインやテーマを使用する
# zplug "modules/osx", from:prezto, if:"[[ $OSTYPE == *darwin* ]]"
# zplug "modules/prompt", from:prezto
# # zstyle は zplug load の前に設定する
# zstyle ':prezto:module:prompt' theme 'sorin'

# インストール・アップデート後に実行されるフック
# この場合は以下のような設定が別途必要
# ZPLUG_SUDO_PASSWORD="********"
zplug "jhawthorn/fzy", \
    as:command, \
    rename-to:fzy, \
    hook-build:"make && sudo make install"

# # リビジョンロック機能を持つ
# zplug "b4b4r07/enhancd", at:v1
# zplug "mollifier/anyframe", at:4c23cb60

# Gist ファイルもインストールできる
zplug "b4b4r07/79ee61f7c140c63d2786", \
    from:gist, \
    as:command, \
    use:get_last_pane_path.sh

# # bitbucket も
# zplug "b4b4r07/hello_bitbucket", \
#     from:bitbucket, \
#     as:command, \
#     use:"*.sh"

# # `use` タグでキャプチャした文字列でリネームする
# zplug "b4b4r07/httpstat", \
#     as:command, \
#     use:'(*).sh', \
#     rename-to:'$1'

# # 依存管理
# # "emoji-cli" は "jq" があるときにのみ読み込まれる
# zplug "stedolan/jq", \
#     from:gh-r, \
#     as:command, \
#     rename-to:jq
# zplug "b4b4r07/emoji-cli", \
#     on:"stedolan/jq"

# ノート: 読み込み順序を遅らせるなら defer タグを使いましょう
# 読み込み順序を設定する
# 例: "zsh-syntax-highlighting" は compinit の後に読み込まれる必要がある
# （2 以上は compinit 後に読み込まれるようになる）
zplug "zsh-users/zsh-syntax-highlighting", defer:2

# ローカルプラグインも読み込める
# zplug "~/.zsh", from:local

# テーマファイルを読み込む
zplug 'dracula/zsh', as:theme

# 未インストール項目をインストールする
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# コマンドをリンクして、PATH に追加し、プラグインは読み込む
zplug load

export PATH="$HOME/.composer/vendor/bin:/usr/local/bin:$PATH"

# export NVM_DIR="$HOME/.nvm"
# if [ -f "/opt/homebrew/opt/nvm/nvm.sh" ]; then
#   [ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && \. "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
# else
#   [ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
#   [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
# fi
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
eval "$(direnv hook zsh)"

export LIBRARY_PATH=$LIBRARY_PATH:/opt/homebrew/opt/openssl@3.2/lib/
export PATH="/opt/homebrew/opt/openjdk@11/bin:$PATH"
export PATH="./node_modules/.bin:./bin:$PATH"
export PATH="/Applications/VMware Fusion.app/Contents/Library/VMware OVF Tool:$PATH"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export FZF_FIND_FILE_COMMAND="ag -l --hidden --ignore .git . \$dir 2> /dev/null"

# Add pyenv executable to PATH and
# enable shims by adding the following
# to ~/.profile and ~/.zprofile:

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PATH:$PYENV_ROOT/bin"
eval "$(pyenv init -)"
if which pyenv-virtualenv-init > /dev/null; then eval "$(pyenv virtualenv-init -)"; fi

export PATH="$HOME/.poetry/bin:$PATH"
export HOMEBREW_NO_AUTO_UPDATE=1

# fix mysqlclient's error
# https://github.com/PyMySQL/mysqlclient/issues/584
export MYSQLCLIENT_LDFLAGS=$(pkg-config --libs mysqlclient)
export MYSQLCLIENT_CFLAGS=$(pkg-config --cflags mysqlclient)

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
export PATH="/opt/homebrew/opt/curl/bin:$PATH"
export CURL_CA_BUNDLE=/etc/ssl/certs/cacert.pem
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
