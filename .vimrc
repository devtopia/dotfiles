" 日本語文字コード自動判別と文字コード交換
" vimが内部で用いるエンコーディングを指定
set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Nerd\ Font\ Complete\ 12
set encoding=utf-8
scriptencoding utf-8

" 端末の出力に用いられるエンコーディングを指定
set termencoding=utf-8

" ファイルを開く時、適切なエンコーディングを自動的に判定
set fileencodings=utf-8,iso-2022-jp,euc-jp,sjis

" 想定される改行の種類を指定
set fileformats=unix,mac,dos

" ○や□の文字が崩れる問題を回避
" set ambiwidth=double

" Enable CursorLine
set cursorline

let mapleader = "\<Space>"

" Manage plugins
call plug#begin('~/.vim/plugged')
Plug 'flazz/vim-colorschemes'
Plug 'itchyny/dictionary.vim'
Plug 'vim-jp/vimdoc-ja-working'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'ConradIrwin/vim-bracketed-paste'
Plug 'honza/vim-snippets'
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'Shougo/echodoc.vim'
Plug 'Shougo/neco-vim'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'cohama/agit.vim'
Plug 'thinca/vim-ref'
Plug 'slim-template/vim-slim', { 'for': 'slim' }
Plug 'tpope/vim-endwise'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-dispatch'
Plug 'jwalton512/vim-blade', { 'for': ['php', 'blade'] }
Plug 'tpope/vim-rails', { 'for': ['ruby', 'eruby'] }
Plug 'vim-ruby/vim-ruby', { 'for': ['ruby', 'eruby'] }
Plug 'depuracao/vim-rdoc', { 'for': ['ruby', 'eruby'] }
Plug 'pangloss/vim-javascript', { 'for': ['javascript', 'javascriptreact', 'typescript', 'typescriptreact']  }
Plug 'othree/yajs.vim', { 'for': ['javascript', 'javascriptreact', 'typescript', 'typescriptreact'] }
Plug 'othree/es.next.syntax.vim', { 'for': ['javascript', 'javascriptreact', 'typescript', 'typescriptreact'] }
Plug 'othree/javascript-libraries-syntax.vim', { 'for': ['javascript', 'javascriptreact', 'typescript', 'typescriptreact'] }
Plug 'leafgarland/typescript-vim', { 'for': ['typescript', 'typescriptreact'] }
Plug 'maxmellon/vim-jsx-pretty', { 'for': ['javascript', 'javascriptreact', 'typescript', 'typescriptreact'] }
Plug 'elzr/vim-json'
Plug 'hail2u/vim-css3-syntax'
Plug 'ap/vim-css-color'
Plug 'Lokaltog/vim-easymotion'
Plug 'thinca/vim-quickrun'
Plug 'plasticboy/vim-markdown'
Plug 'timcharper/textile.vim'
Plug 'tyru/open-browser.vim'
Plug 'previm/previm'
Plug 'tsaleh/vim-tmux'
Plug 'jiangmiao/auto-pairs'
Plug 'tomtom/tcomment_vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
Plug 'pearofducks/ansible-vim'
Plug 'neoclide/coc-neco'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'yaegassy/coc-ansible', {'do': 'yarn install --frozen-lockfile'}
Plug 'roxma/nvim-yarp'
Plug 'roxma/vim-hug-neovim-rpc'
Plug 'dense-analysis/ale'
Plug 'ryanoasis/vim-devicons'
Plug 'devtopia/sudo.vim'
Plug 'edkolev/tmuxline.vim'
Plug 'andymass/vim-matchup'
Plug 'udalov/kotlin-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'rizzatti/dash.vim'
Plug 'puremourning/vimspector'
Plug 'vim-test/vim-test'
Plug 'styled-components/vim-styled-components', { 'branch': 'main' }
Plug 'vim-scripts/taglist.vim'
Plug 'vim-scripts/BufOnly.vim'
Plug 'pantharshit00/vim-prisma'
Plug 'sheerun/vim-polyglot'
Plug 'maksimr/vim-jsbeautify'
Plug 'MarcWeber/vim-addon-local-vimrc'
" Plug 'Exafunction/codeium.vim', { 'branch': 'main' }

" Add plugins to &runtimepath
call plug#end()

" Configure plugins
augroup MyVimrc
  autocmd!
augroup END

" https://github.com/itchyny/dictionary.vim
map <silent> ,d :Dictionary<CR>

" https://github.com/othree/javascript-libraries-syntax.vim
function! EnableJavascript()
  " Setup used libraries
  let g:used_javascript_libs = 'jquery,underscore,react,flux,jasmine,d3'
  let b:javascript_lib_use_jquery = 1
  let b:javascript_lib_use_underscore = 1
  let b:javascript_lib_use_react = 1
endfunction
autocmd MyVimrc FileType javascript,javascriptreact,typescript,typescriptreact call EnableJavascript()

" https://github.com/slim-template/vim-slim
autocmd MyVimrc BufNewFile,BufRead *.slim setlocal filetype=slim

" https://github.com/scrooloose/nerdtree
map <silent> <C-e> :NERDTreeToggle<CR>
let g:NERDTreeWinPos    = "left"
let g:NERDTreeDirArrows = 1
let g:NERDTreeMinimalUI = 1
let g:NERDTreeShowHidden = 1
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" https://github.com/easymotion/vim-easymotion
" s{char}{char} to move to {char}{char}
nmap s <Plug>(easymotion-overwin-f2)
let g:EasyMotion_startofline = 0 " keep cursor column when JK motion

" ctags
set tags+=.git/tags
set tags+=.git/library.tags
function! CreateCtagWithRuby()
  nmap <silent> <F1> :!ctags -R --tag-relative --languages=Ruby --sort=yes --exclude=log --exclude=.git --exclude=vendor --exclude=.bundle -f".git/tags" 2>/dev/null<CR>
  nmap <silent> <F2> :!ctags -R --tag-relative --languages=Ruby --sort=yes --exclude=.git -f".git/library.tags" `bundle list --paths` 2>/dev/null<CR>
endfunction
autocmd MyVimrc FileType ruby,eruby call CreateCtagWithRuby()

function! CreateCtagWithPython()
  nmap <silent> <F1> :!ctags -R --tag-relative --languages=Python --exclude=.git -f".git/tags" 2>/dev/null<CR>
  nmap <silent> <F2> :!ctags -R --tag-relative --languages=Python --exclude=.git -f".git/library.tags" `python -c "import os, sys; print(' '.join('{}'.format(d) for d in sys.path if os.path.isdir(d)))"` 2>/dev/null<CR>
endfunction
autocmd MyVimrc FileType python call CreateCtagWithPython()

" function! CreateCtagWithTypeScript()
"   nmap <silent> <F1> :!ctags -R --tag-relative --languages=TypeScript --langmap=TypeScript:.ts.tsx --exclude=.git --exclude=node_modules --exclude=public --exclude=dist -f".git/tags" 2>/dev/null<CR>
"   nmap <silent> <F2> :!ctags -R --tag-relative --languages=TypeScript --langmap=TypeScript:.ts.tsx --exclude=.git --exclude=src --exclude=test --exclude=tests --exclude=public --exclude=dist --exclude=pages --exclude=types --exclude=__tests__ -f".git/library.tags" 2>/dev/null<CR>
" endfunction
" autocmd MyVimrc FileType typescript,typescriptreact call CreateCtagWithTypeScript()

" taglist
let Tlist_Auto_Update = 1
let Tlist_Exit_OnlyWindow = 1
let Tlist_File_Fold_Auto_Close = 1
let Tlist_Use_Right_Window = 1
nmap <silent> tl :Tlist<CR>

" https://github.com/vim-test/vim-test
nmap <silent> ,t :TestNearest<CR>
nmap <silent> ,f :TestFile<CR>
nmap <silent> ,a :TestSuite<CR>
nmap <silent> ,l :TestLast<CR>
nmap <silent> ,g :TestVisit<CR>
" make test commands execute using dispatch.vim
" let test#strategy = "dispatch"
let test#strategy = "vimterminal"
let g:test#javascript#jest#file_pattern = '\v(tests/.*|test)\.(js|jsx|coffee|ts|tsx)$'
let g:test#javascript#jest#executable = 'yarn jest'
let g:test#javascript#playwright#file_pattern = '\v(e2e/.*|spec)\.(js|jsx|coffee|ts|tsx)$'
let g:test#javascript#playwright#executable = 'yarn playwright'
let g:test#javascript#cypress#file_pattern = '\v(cypress/e2e/.*|cy)\.(js|jsx|coffee|ts|tsx)$'
let g:test#javascript#cypress#executable = 'cypress'
let test#python#runner = "pytest"
let test#python#pytest#options = "-v -s --ff --tb=line"
let test#ruby#rspec#executable = "rspec"
let test#ruby#rspec#options = {
  \ 'nearest': '--backtrace',
  \ 'file':    '--format documentation',
  \ 'suite':   '--tag ~slow',
\}

" https://github.com/thinca/vim-quickrun
nmap <silent> ,r :QuickRun<CR>
set splitright
let g:quickrun_config = {
 \   "_" : {
 \   "outputter":"buffered",
 \   "outputter/buffered/target":"buffer",
 \   "outputter/buffer/opener":"vnew",
 \   "hook/time/enable":"1",
 \   }
 \}

 let g:quickrun_config['typescript'] = { 'type' : 'typescript/tsc' }
 let g:quickrun_config['typescript/tsc'] = {
 \   'command': 'tsc',
 \   'exec': ['%c --target esnext --module commonjs %o %s', 'node %s:r.js'],
 \   'tempfile': '%{tempname()}.ts',
 \   'hook/sweep/files': ['%S:p:r.js'],
 \ }

" Configure markdown
autocmd MyVimrc BufRead,BufNewFile *.{md,mdown,mkd,mkdn,mark*} set filetype=markdown
autocmd MyVimrc BufRead,BufNewFile *.textile set filetype=textile

" Set filetype for React
autocmd bufnewfile,bufread *.tsx set filetype=typescriptreact
autocmd bufnewfile,bufread *.jsx set filetype=javascriptreact

let g:vim_markdown_new_list_item_indent = 2
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_fenced_languages = [
    \ 'viml=vim',
    \ 'help',
    \ 'bash=sh',
    \ 'ini=dosini',
    \ 'ruby',
    \ 'javascript',
    \ 'typescript.tsx=javascript',
    \ 'typescript.ts=javascript',
    \ 'python',
    \ 'json'
    \]
let g:previm_open_cmd = 'open -a Google\ Chrome'
let g:previm_show_header = 0
" let g:previm_disable_default_css = 1
" let g:previm_custom_css_path = '~/dotfiles/templates/previm/markdown.css'
map ,w :PrevimOpen<CR>

" Powerline系フォントを利用する
set laststatus=2
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#whitespace#mixed_indent_algo = 1
" let g:airline_section_error = '%{ALEGetError()}'
" let g:airline_section_warning = '%{ALEGetWarning()}'
" let g:airline_theme = 'papercolor'
let g:airline_theme = 'solarized'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.maxlinenr = '㏑'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''

function! ALEGetError()
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  return l:counts.total == 0 ? '' : printf('%dE', all_errors)
endfunction

function! ALEGetWarning()
  let l:counts = ale#statusline#Count(bufnr(''))
  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors
  return l:counts.total == 0 ? '' : printf('%dW', all_non_errors)
endfunction
" let airline#extensions#coc#error_symbol = 'Error:'
" let airline#extensions#coc#error_symbol = 'Warning:'
" let airline#extensions#coc#stl_format_err = '%E{[%e(#%fe)]}'
" let airline#extensions#coc#stl_format_warn = '%W{[%w(#%fw)]}'

" tmuxline
let g:airline#extensions#tmuxline#enabled = 0
let g:tmuxline_theme = 'vim_statusline_3'
let g:tmuxline_preset = {
      \'a'    : '#S',
      \'c'    : ['#(whoami)'],
      \'win'  : ['#I', '#W'],
      \'cwin' : ['#I', '#W', '#F'],
      \'x'    : ['#{battery_status_fg}#{battery_percentage} #{battery_icon_charge}#{cpu_fg_color}#{cpu_percentage} #{cpu_icon}'],
      \'y'    : ['%a', '%R', '#(ansiweather | cut -d " " -f5,6,7)'],
      \'z'    : '#H',
      \'options' : {'status-justify':'left'}}

" https://github.com/nathanaelkane/vim-indent-guides
" vim立ち上げたときに、自動的にvim-indent-guidesをオンにする
let g:indent_guides_enable_on_vim_startup = 1
" ガイドをスタートするインデントの量
let g:indent_guides_start_level           = 2
" 自動カラーを無効にする
let g:indent_guides_auto_colors           = 0
" ハイライト色の変化の幅
let g:indent_guides_color_change_percent  = 30
" ガイドの幅
let g:indent_guides_guide_size            = 1

" Completion
" Enable omni completion.
" autocmd MyVimrc FileType javascript,typescript setlocal omnifunc=tern#Complete
autocmd MyVimrc FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
" autocmd MyVimrc FileType yaml.ansible set filetype=yaml

" echodoc can handle the function signatures displaying
set cmdheight=2
let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'signature'

" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }

" In Neovim, you can set up fzf window using a Vim command
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_layout = { 'window': '-tabnew' }
let g:fzf_layout = { 'window': '10split' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.local/share/fzf-history'

" Similarly, we can apply it to fzf#vim#grep. To use ripgrep instead of ag:
command! -bang -nargs=* Rg
\ call fzf#vim#grep(
\ 'rg -g '!dist' -g '!.git' -g '!node_modules' --column --line-number --hidden --ignore-case --no-heading --color=always '.shellescape(<q-args>), 1,
\ <bang>0 ? fzf#vim#with_preview({'options': '--delimiter : --nth 4..'}, 'up:60%')
\ : fzf#vim#with_preview({'options': '--exact --delimiter : --nth 4..'}, 'right:50%:hidden', '?'),
\ <bang>0)

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   "rg -g '!design/' -g '!dist/' -g '!pnpm-lock.yaml' -g '!.git' -g '!node_modules' --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1,
  \   fzf#vim#with_preview({'options': '--exact --delimiter : --nth 4..'}), <bang>0)

" Mapping selecting mappings
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)

" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-j> <plug>(fzf-complete-file-ag)
imap <c-x><c-l> <plug>(fzf-complete-line)

" Advanced customization using autoload functions
imap <expr> <c-x><c-k> fzf#vim#complete#word({'left': '15%'})

" Mapping fzf commands
nmap <Leader>f :Files<CR>
nmap <Leader>g :GFiles<CR>
nmap <Leader>b :Buffers<CR>
nmap <Leader>l :Lines<CR>
nmap <Leader>t :Tags<CR>
nmap <Leader>c :Commands<CR>
nmap <Leader>r :Rg<CR>

" https://github.com/tpope/vim-rails
function! SetVimRails()
  let g:rails_default_file='config/database.yml'
  let g:rails_level=4
  let g:rails_mappings=1
  let g:rails_modelines=0

  nmap <buffer>rr :R<CR>
  nmap <buffer>ra :A<CR>
  nmap <buffer>rm :Emodel<CR>
  nmap <buffer>rc :Econtroller<CR>
  nmap <buffer>rv :Eview<CR>
  nmap <buffer>rh :Ehelper<CR>
  nmap <buffer>rs :Espec<CR>
  nmap <buffer>ri :Einitializer<CR>
endfunction
autocmd MyVimrc User Rails call SetVimRails()

" https://github.com/thinca/vim-ref
let g:ref_refe_cmd = 'refe' "refeコマンドのパス

" 読み込んだプラグインも含め、ファイルタイプを検出、
" ファイルタイプ別プラグイン、インデントを有効化する。
filetype plugin indent on

" 256カラーを使う。
set t_Co=256
set term=screen-256color

" 選択した範囲のインデントサイズを連続変更
vnoremap < <gv
vnoremap > >gv

" 構文ハイライト有効
syntax on
set synmaxcol=200
set background=light
" colorscheme molokai
" colorscheme badwolf
colorscheme solarized8_light

" インクリメンタル検索を有効
set incsearch

" 検索のとき大小文字無視
set ignorecase

" 検索ハイライト有効
set hlsearch

" ex)
" foobar: 大小文字区分しない。
" FOOBAR: される。
" FooBar: される。
set smartcase

" ESCキー連打でさりげなく色を消す。
nmap <Esc><Esc> :nohlsearch<CR><Esc>

" 折り返しをする。
set wrap
" markdown,textileの場合は行末で折り返さない。
" autocmd MyVimrc FileType markdown,textile set nowrap

" オートインデントを使用するため必要
set nopaste

" 行番号を表示する。
set number

" カーソルを左右に移動させる。
set whichwrap=b,s,h,l,[,],<,>,~

" Configure indent
" タブ入れを複数の空白入力に置き換える。
set expandtab

" 画面上でタグ文字が占める幅
set tabstop=2

" 自動インデントでずれる幅
set shiftwidth=2

" 連続した空白に対してタグキーやバックスペースキーでカーソルが動く幅
set softtabstop=2

" 改行の時に前の行のインデントを継続する。
set autoindent

" 改行の時に入力された行の末尾に合わせて次の行のインデントを増減する。
set smartindent

" autocmd MyVimrc FileType htmldjango  setlocal ts=2 sts=2 sw=2 et
" autocmd MyVimrc FileType python      setlocal ts=4 sts=4 sw=4 et

" mouse
" normal(n), visual(v), insert(i), command(c) = all(a)
set mouse=a

" Configure enable mouse drag
set ttymouse=xterm2

" backspace
" start: インサートモードの開始位置より前の文字列の削除を許可する。。
" indent: 自動インデントを行った場合、インデント部分の削除を許可する。
" eol: 改行文字の削除を許可する。
set backspace=start,indent,eol

" Configure vim's directory
set backup
set swapfile
set backupdir=~/.vim/backup
set directory=~/.vim/swap

" Configure vim's folding
" zc: close, zo: open, zC: close all, zO: open all
set foldmethod=syntax
set foldlevel=100
autocmd MyVimrc InsertEnter * if !exists('w:last_fdm')
  \| let w:last_fdm=&foldmethod
  \| setlocal foldmethod=manual
  \| endif

autocmd MyVimrc InsertLeave,WinLeave * if exists('w:last_fdm')
  \| let &l:foldmethod=w:last_fdm
  \| unlet w:last_fdm
  \| endif

" Reload vimrc file
noremap <Space>. :<C-u>edit $MYVIMRC<CR>
noremap <Space>s. :<C-u>source $MYVIMRC<CR>

let g:WebDevIconsNerdTreeBeforeGlyphPadding = ""
let g:WebDevIconsUnicodeDecorateFolderNodes = v:true
" after a re-source, fix syntax matching issues (concealing brackets):
if exists('g:loaded_webdevicons')
  call webdevicons#refresh()
endif

" Make comment color to lightseagreen
hi Comment ctermfg=37

" Make color when visual mode used
hi Visual ctermfg=Black ctermbg=229

" Make colorscheme to dark color
" autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=237
" autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=235
" hi colorcolumn ctermbg=235
" Make colorscheme to light color
autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=255
autocmd MyVimrc VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=254
hi colorcolumn ctermbg=253

" Display vertical line at 80 columns
" execute "set colorcolumn=" . join(range(81, 81), ',')

" Remove all white space in a file
autocmd MyVimrc BufWritePre * :%s/\s\+$//ge

" Configure short command for buffer
nmap <silent>bp :bprevious<CR>
nmap <silent>bn :bnext<CR>
nmap <silent>bb :b#<CR>
nmap <silent>bf :bfirst<CR>
nmap <silent>bl :blast<CR>
nmap <silent>bm :bm<CR>
nmap <silent>bd :bp\|bw #<CR>
nmap <silent>bo :BufOnly<CR>

let g:coc_global_extensions = [
      \'coc-lists',
      \'coc-dictionary',
      \'coc-tag',
      \'coc-emoji',
      \'coc-json',
      \'coc-solargraph',
      \'coc-yaml',
      \'coc-css',
      \'coc-flutter',
      \'coc-highlight',
      \'coc-html',
      \'coc-htmldjango',
      \'coc-htmlhint',
      \'coc-html-css-support',
      \'@yaegassy/coc-tailwindcss3',
      \'coc-tsserver',
      \'coc-stylelint',
      \'coc-prettier',
      \'coc-prisma',
      \'coc-pyright',
      \'coc-restclient',
      \'coc-snippets',
      \'coc-styled-components',
\]
" coc.nvim
" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1):
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>fs  <Plug>(coc-format-selected)
nmap <leader>fs  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Use <C-l> for trigger snippet expand.
imap <C-l> <Plug>(coc-snippets-expand)

" Use <C-j> for select text for visual placeholder of snippet.
vmap <C-j> <Plug>(coc-snippets-select)

" Use <C-j> for jump to next placeholder, it's default of coc.nvim
let g:coc_snippet_next = '<c-j>'

" Use <C-k> for jump to previous placeholder, it's default of coc.nvim
let g:coc_snippet_prev = '<c-k>'

" Use <C-j> for both expand and jump (make expand higher priority.)
" imap <C-j> <Plug>(coc-snippets-expand-jump)"

" let g:coc_node_args = ['--nolazy', '--inspect-brk=6045']

nmap <silent> ga <Plug>(coc-codeaction-line)
nmap <silent> df :call CocAction('format')<CR>

let g:coc_filetype_map = {
  \ 'yaml.ansible': 'ansible',
  \ }
" https://prettier.io/docs/en/vim
command! -nargs=0 Prettier :call CocAction('runCommand', 'prettier.formatFile')

" https://github.com/pr4th4m/coc-restclient
noremap <Leader>0 :CocCommand rest-client.request <cr>

" aleの設定
" 保存時のみ実行する
let g:ale_lint_on_text_changed = 'never'

" 表示に関する設定
let g:ale_sign_error = ''
let g:ale_sign_warning = ''
highlight ALEErrorSign   ctermbg=NONE ctermfg=red
highlight ALEWarningSign ctermbg=NONE ctermfg=yellow
let g:ale_linters_explicit = 1
let g:ale_lint_on_save = 1
let g:ale_fix_on_save = 1
" let g:ale_completion_enabled = 1
let g:python3_bin_path = $PYENV_ROOT . '/versions/vim9/bin/'
" let g:ale_javascript_prettier_options = '--no-semi --single-quote --trailing-comma none'

" Ctrl + kで次の指摘へ、Ctrl + jで前の指摘へ移動
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)
nmap <silent>ai :ALEInfo<CR>
nmap <silent>af :ALEFix<CR>
nmap <silent>afs :ALEFixSuggest<CR>

" ロケーションリストの代わりにQuickFixを使う
let g:ale_set_loclist = 0
let g:ale_set_quickfix = 1
" let g:ale_open_list = 1
" let g:ale_keep_list_window_open = 1

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

let g:ale_linter_aliases = {'yaml': ['ansible', 'yaml.ansible']}
let g:ale_kotlin_ktlint_options = '--android'

" 指定のlinterのみ使う。指定されてない場合はデフォルトでほぼ全てのlinterを使う。
let g:ale_linters = {
  \   'javascript': ['eslint'],
  \   'javascriptreact': ['eslint'],
  \   'typescript': ['eslint', 'tsserver'],
  \   'typescriptreact': ['eslint', 'tsserver'],
  \   'ruby': ['solargraph', 'rubocop'],
  \   'eruby': ['erblint'],
  \   'css': ['stylelint'],
  \   'scss': ['stylelint'],
  \   'yaml': ['ansible_lint', 'yamllint'],
  \   'html': ['htmlhint'],
  \   'kotlin': ['ktlint'],
  \   'python': ['flake8', 'pyright'],
  \ }
" 自動で修正を行う。
let g:ale_fixers = {
  \   '*': ['remove_trailing_lines', 'trim_whitespace'],
  \   'ruby': ['prettier', 'rubocop'],
  \   'eruby': ['erblint', 'htmlbeautifier'],
  \   'css': ['stylelint'],
  \   'scss': ['stylelint'],
  \   'html': ['html-beautify'],
  \   'kotlin': ['ktlint'],
  \   'python': ['black', 'isort'],
  \   'json': ['fixjson'],
  \ }

" 各ツールの実行オプションを変更してPythonパスを固定
let g:ale_python_flake8_executable = g:python3_bin_path . 'flake8'
let g:ale_python_flake8_options = g:python3_bin_path . '-m flake8 --ignore=E203 --max-line-length=88'
let g:ale_python_pylint_executable = g:python3_bin_path . 'pylint'
let g:ale_python_pylint_options = '--load-plugins=pylint_django'
let g:ale_python_yapf_executable = g:python3_bin_path . "yapf --style='{based_on_style:google,column_limit:80}'"
let g:ale_python_isort_executable = g:python3_bin_path . 'isort'
let g:ale_python_black_executable = g:python3_bin_path . 'black'
let g:ale_python_pyright_executable = g:python3_bin_path . 'pyright'

" yarn berry
let s:sdks = finddir('.yarn/sdks', ';')
if !empty(s:sdks)
  " let g:ale_javascript_eslint_use_global = 1
  " let g:ale_javascript_eslint_executable = s:sdks . '/eslint/bin/eslint.js'
  " let g:ale_javascript_tsserver_use_global = 1
  " let g:ale_typescript_tsserver_executable = s:sdks . '/typescript/bin/tsserver'
  " let g:ale_javascript_prettier_use_global = 1
  " let g:ale_javascript_prettier_executable = s:sdks . '/prettier/index.js'
  " fix from https://github.com/dense-analysis/ale/issues/2970#issuecomment-588179435
  let g:ale_javascript_eslint_use_global = 1
  let g:ale_javascript_eslint_executable = 'yarn'
  let g:ale_javascript_eslint_options = 'run eslint'
  let g:ale_javascript_prettier_use_global = 1
  let g:ale_javascript_prettier_executable = 'yarn'
  let g:ale_javascript_prettier_options = 'run prettier'
endif

" ruby
let g:ale_ruby_rubocop_executable = 'bin/rubocop'
let g:ale_ruby_solargraph_executable = 'bin/solargraph'
let g:ale_eruby_erblint_executable = "bin/erblint"
let g:ale_eruby_erbformatter_executable = "bin/erb-formatter"
let g:ale_eruby_htmlbeautifier_executable = "bin/htmlbeautifier"

" set jsx-tag
" dark red
hi tsxTagName guifg=#E06C75

" orange
hi tsxCloseString guifg=#F99575
hi tsxCloseTag guifg=#F99575
hi tsxAttributeBraces guifg=#F99575
hi tsxEqual guifg=#F99575

" yellow
hi tsxAttrib guifg=#F8BD7F cterm=italic

hi ReactState guifg=#C176A7
hi ReactProps guifg=#D19A66
hi Events ctermfg=204 guifg=#56B6C2
hi ReduxKeywords ctermfg=204 guifg=#C678DD
hi WebBrowser ctermfg=204 guifg=#56B6C2
hi ReactLifeCycleMethods ctermfg=204 guifg=#D19A66

let g:typescript_indent_disable = 1

" clipboardの内容を即座に貼り付け
set clipboard=unnamed
nmap <leader>y "*y
nmap <leader>p "*p
nmap <leader>Y "+y
nmap <leader>P "+p

let g:php_baselib = 1
let g:php_htmlInStrings = 1
let g:php_noShortTags = 1
let g:php_sql_query = 1
let g:sql_type_default = 'mysql'

" toggles the quickfix window.
command -bang -nargs=? QFix call QFixToggle(<bang>0)
function! QFixToggle(forced)
  if exists("g:qfix_win") && a:forced == 0
    cclose
  else
    execute "copen 10"
  endif
endfunction

" used to track the quickfix window
augroup QFixToggle
 autocmd!
 autocmd BufWinEnter quickfix let g:qfix_win = bufnr("$")
 autocmd BufWinLeave * if exists("g:qfix_win") && expand("<abuf>") == g:qfix_win | unlet! g:qfix_win | endif
augroup END
map <silent> ,q :QFix<CR>

" Define some single Blade directives. This variable is used for highlighting only.
let g:blade_custom_directives = ['datetime', 'javascript']

" Define pairs of Blade directives. This variable is used for highlighting and indentation.
let g:blade_custom_directives_pairs = {
      \   'markdown': 'endmarkdown',
      \   'cache': 'endcache',
      \ }

let g:vimspector_enable_mappings = 'HUMAN'
nnoremap <Leader>dd :call vimspector#Launch()<CR>
nnoremap <Leader>dr :call vimspector#Restart()<CR>
nnoremap <Leader>de :call vimspector#Reset()<CR>
nnoremap <Leader>dc :call vimspector#Continue()<CR>
nnoremap <Leader>dt :call vimspector#ToggleBreakpoint()<CR>
nnoremap <Leader>dT :call vimspector#ClearBreakpoints()<CR>
nmap <Leader>dk <Plug>VimspectorRestart
nmap <Leader>dh <Plug>VimspectorStepOut
nmap <Leader>dl <Plug>VimspectorStepInto
nmap <Leader>dj <Plug>VimspectorStepOver
" sign define vimspectorBP text=🔴 texthl=Normal
" sign define vimspectorBPDisabled text=🔵 texthl=Normal
" sign define vimspectorPC text=🔶 texthl=SpellBad

map <silent> gt :GitGutterToggle<CR>
map <c-f> :call JsBeautify()<cr>

" " codeium
" set statusline+=\{…\}%3{codeium#GetStatusString()}
" let g:codeium_disable_bindings = 1
" imap <script><silent><nowait><expr> <C-g> codeium#Accept()
" imap <C-n>   <Cmd>call codeium#CycleCompletions(1)<CR>
" imap <C-p>   <Cmd>call codeium#CycleCompletions(-1)<CR>
" imap <C-x>   <Cmd>call codeium#Clear()<CR>
" map <silent> ,c :call codeium#Chat()<CR>
